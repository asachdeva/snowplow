FROM openjdk:8u171-jre-alpine3.8
MAINTAINER Akshay Sachdeva <asachdeva@utexas.edu>

RUN apk add --update --no-cache \
    libc6-compat

WORKDIR /app

# NOTE:
# Do NOT change this command.
# Modify .dockerignore in order to change what gets copied into this image.
COPY . .

ENV BIND_ADDR=${BIND_ADDR:-0.0.0.0}
ENV BIND_PORT=${BIND_PORT:-80}

EXPOSE ${BIND_PORT}

ENTRYPOINT ["java", "-jar", "target/scala-2.12/JsonSchemaService-assembly-0.1.0-SNAPSHOT.jar"]
