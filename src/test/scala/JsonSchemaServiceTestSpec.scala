import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor
import repository._
import org.http4s.dsl.io._
import org.scalatest._
import org.http4s.circe._
import org.http4s._
import org.http4s.implicits._
import cats.effect._
import db._
import io.circe.Json
import service._
import io.circe.literal._

class JsonSchemaServiceTestSpec extends FunSpec with doobie.scalatest.IOChecker {
  implicit val cs                                             = IO.contextShift(ExecutionContexts.synchronous)
  implicit val responseDecoder                                = jsonOf[IO, Responses.Response]
  implicit val schemaEncoder: EntityEncoder[IO, Option[Json]] = jsonEncoderOf[IO, Option[Json]]

  val repo                = SchemaRepositoryF.apply[IO](transactor).unsafeRunSync
  val service             = new SchemaService().service(repo)
  val sampleValidSchema   = json"""{"type": "number"}"""
  val sampleInvalidSchema = json"""{"type": "foobar"}"""
  val sampleValidJson     = json"""7"""
  val sampleInValidJson   = json"""{"foo": "bar"}"""

  describe("when you setup the database DDL runs correctly") {
    describe("for provided DDL") {

      it("should have 0 errors") {
        check(SqlStatements.ddl)
      }
    }
  }

  describe("Repository and Service tests") {
    Database.createTables(transactor).unsafeRunSync
    describe("when the db is empty") {
      it("should return None") {
        val repo   = SchemaRepositoryF.apply[IO](transactor).unsafeRunSync
        val schema = repo.getSchema("foo").unsafeRunSync

        assert(schema == None)

        val request  = Request[IO](GET, uri"/schema/foo")
        val response = service.orNotFound.run(request).unsafeRunSync()

        assert(response.status == NotFound)
      }
    }

    describe("correctly persist schema") {
      describe("when valid sample json is provided ") {
        it("should return Ok") {

          repo.addSchema("test_schema_1", sampleValidSchema.toString()).unsafeRunSync

          val theSchema = repo.getSchema("test_schema_1").unsafeRunSync
          val request   = Request[IO](GET, uri"/schema/test_schema_1")
          val response  = service.orNotFound.run(request).unsafeRunSync

          assert(response.status == Ok)
          assert(theSchema.isDefined)
          assert(response.as[Json].unsafeRunSync == sampleValidSchema)
        }
      }
    }

    describe("generate correct response status on POST") {
      describe("when valid sample json schema is provided ") {
        it("should return Created") {
          val request  = Request[IO](POST, uri"/schema/test_schema_2").withEntity(sampleValidSchema)
          val response = service.orNotFound.run(request).unsafeRunSync

          assert(response.status == Created)
        }
      }
    }

    describe("generate error response status on POST") {
      describe("when invalid sample json schema is provided ") {
        it("should return BadRequest") {
          val request  = Request[IO](POST, uri"/schema/test_schema_3").withEntity(sampleInvalidSchema)
          val response = service.orNotFound.run(request).unsafeRunSync

          assert(response.status == BadRequest)
        }
      }
    }

    describe("generate correct response status on POST for valid schema and valid json") {
      describe("when valid sample json is provided ") {
        it("should return  Ok") {
          val request  = Request[IO](POST, uri"/validate/test_schema_2").withEntity(sampleValidJson)
          val response = service.orNotFound.run(request).unsafeRunSync

          assert(response.status == Ok)
        }
      }
    }

    describe("generate error response status on POST for valid schema and invalid json") {
      describe("when invalid json is provided ") {
        it("should return BadRequest") {
          val request  = Request[IO](POST, uri"/validate/test_schema_2").withEntity(sampleInValidJson)
          val response = service.orNotFound.run(request).unsafeRunSync

          assert(response.status == BadRequest)
        }
      }
    }

    describe("generate 404 on POST for not present schema") {
      describe("when valid json is provided ") {
        it("should return NotFound") {
          val request  = Request[IO](POST, uri"/validate/test_schema_77").withEntity(sampleInValidJson)
          val response = service.orNotFound.run(request).unsafeRunSync

          assert(response.status == NotFound)
        }
      }
    }
  }

  override def transactor: doobie.Transactor[IO] = Transactor.fromDriverManager[IO](
    "org.h2.Driver", // driver classname
    "jdbc:h2:mem:todo;MODE=PostgreSQL;DB_CLOSE_DELAY=-1", // connect URL (driver-specific)
    "sa", // user
    "", // password
    ExecutionContexts.synchronous // just for testing
  )
}
