package service
import cats.implicits._
import cats.effect._
import io.circe._
import io.circe.syntax._
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl._
import repository.SchemaRepository
import validation.JsonSchemaValidation

import scala.collection.JavaConverters._

final class SchemaService[F[_]] extends Http4sDsl[F] {
  implicit val schemaEncoder: EntityEncoder[IO, Option[Json]] = jsonEncoderOf[IO, Option[Json]]

  def service(schemaRepo: SchemaRepository[F])(implicit F: ConcurrentEffect[F]): HttpRoutes[F] =
    HttpRoutes
      .of[F] {
        case GET -> Root / "schema" / schemaId =>
          schemaRepo.getSchema(schemaId).flatMap {
            case Some(schema) => Ok(schema)
            case None         => NotFound()
          }
        case req @ POST -> Root / "schema" / schemaId =>
          req
            .as[Json]
            .map(JsonSchemaValidation.validateSchema)
            .flatMap {
              case (report, schema) =>
                if (report.isSuccess) {
                  schemaRepo.addSchema(schemaId, schema.toString).flatMap { saved =>
                    if (saved)
                      Created(Responses.success("uploadSchema", schemaId).asJson)
                    else
                      BadRequest()
                  }
                } else {
                  val errors = report.iterator.asScala.toList.map(_.toString)
                  BadRequest(Responses.error("uploadSchema", schemaId, errors: _*).asJson)
                }
            }
        case req @ POST -> Root / "validate" / schemaId =>
          schemaRepo.getSchema(schemaId).flatMap {
            case Some(schema) =>
              req
                .as[Json]
                .map(dropNulls)
                .map(JsonSchemaValidation.validateJson(schema, _))
                .flatMap { report =>
                  if (report.isSuccess) {
                    Ok(Responses.success("validateDocument", schemaId).asJson)
                  } else {
                    val errors = report.iterator.asScala.toList.map(_.toString)
                    BadRequest(Responses.error("validateDocument", schemaId, errors: _*).asJson)
                  }
                }
            case None =>
              NotFound()
          }
      }

  private def dropNulls(json: Json): Json =
    json.mapObject(_.filter {
      case (_, Json.Null) => false
      case _              => true
    }.mapValues(dropNulls))
}
