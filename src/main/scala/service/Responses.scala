package service

import io.circe.Decoder
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.semiauto._
import io.circe.literal._

object Responses {
  implicit val circeConfig: Configuration = Configuration.default.withDefaults

  implicit val responseEncoder = deriveEncoder[Response].mapJsonObject(
    _.filter {
      case ("messages", obj) => obj != json"[]"
      case _                 => true
    }
  )

  implicit val responseDecoder: Decoder[Response] = deriveDecoder[Response]

  case class Response(action: String, id: String, status: String, messages: List[String] = List())

  def success(action: String, id: String, messages: String*): Response =
    Response(action, id, "success", messages.toList)

  def error(action: String, id: String, messages: String*): Response =
    Response(action, id, "error", messages.toList)
}
