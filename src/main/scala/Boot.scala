import cats.effect._
import cats.implicits._
import config._
import db._
import service.SchemaService
import io.circe._
import org.http4s.server.blaze._
import org.http4s.circe._
import org.http4s._
import org.http4s.implicits._
import org.http4s.server.Router
import repository.SchemaRepositoryF

object Boot extends IOApp {
  implicit val schemaEncoder: EntityEncoder[IO, Option[Json]] = jsonEncoderOf[IO, Option[Json]]

  def createServer[F[_]: ContextShift: ConcurrentEffect: Timer]: Resource[F, ExitCode] =
    for {
      config        <- Resource.liftF(Config.load[F]())
      tx            <- Database.hikariTransactor[F](config.database)
      schemaRepo    <- Resource.liftF(SchemaRepositoryF.apply[F](tx))
      schemaService = new SchemaService().service(schemaRepo)
      httpApp       = Router("/" -> schemaService).orNotFound

      _ <- Resource.liftF(Database.migrate(config.database))

      exitCode <- Resource.liftF(
                   BlazeServerBuilder[F]
                     .bindHttp(config.server.port, config.server.host)
                     .withHttpApp(httpApp)
                     .serve
                     .compile
                     .drain
                     .as(ExitCode.Success)
                 )

    } yield exitCode

  override def run(args: List[String]): IO[ExitCode] =
    createServer.use(IO.pure) // Use IO as the effect type
}
