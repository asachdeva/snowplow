package repository

import cats.effect._
import cats.implicits._
import doobie._
import doobie.implicits._
import doobie.util.query.Query0
import doobie.util.update.Update0
import io.circe.Json
import io.circe.parser._

final class SchemaRepositoryF[F[_]: ConcurrentEffect](tx: Transactor[F]) extends SchemaRepository[F] {
  override def addSchema(id: String, schema: String): F[Boolean] =
    SqlStatements
      .addSchema(id, schema)
      .run
      .transact(tx)
      .map(num => num == 1)

  override def getSchema(id: String): F[Option[Json]] =
    SqlStatements
      .getSchema(id)
      .map(str => (parse(str).right.get))
      .option
      .transact(tx)
}

object SchemaRepositoryF {
  def apply[F[_]](tx: Transactor[F])(implicit F: ConcurrentEffect[F]): F[SchemaRepositoryF[F]] =
    F.pure { new SchemaRepositoryF(tx) }
}

object SqlStatements {
  val ddl: Update0 =
    sql"""
       CREATE TABLE SCHEMAS (id VARCHAR(20) PRIMARY KEY NOT NULL,
       json TEXT NOT NULL);""".update

  def getSchema(id: String): Query0[String] =
    sql"""SELECT json FROM SCHEMAS where id = $id"""
      .query[String]

  def addSchema(id: String, json: String): Update0 =
    sql"""INSERT INTO SCHEMAS (id, json) VALUES ($id, $json)""".update
}
