package repository

import io.circe.Json

trait SchemaRepository[F[_]] {
  def addSchema(id: String, schema: String): F[Boolean]
  def getSchema(id: String): F[Option[Json]]
}
