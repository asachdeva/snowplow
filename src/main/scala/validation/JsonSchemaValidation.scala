package validation

import com.github.fge.jackson.JsonLoader
import com.github.fge.jsonschema.core.report.ProcessingReport
import com.github.fge.jsonschema.main.JsonSchemaFactory
import io.circe.Json

object JsonSchemaValidation {

  def validateSchema(schema: Json): (ProcessingReport, Json) = {
    val report = JsonSchemaFactory.byDefault.getSyntaxValidator
      .validateSchema(JsonLoader.fromString(schema.toString))
    (report, schema)
  }

  def validateJson(validSchema: Json, jsonToValidate: Json): ProcessingReport = {
    val schema = JsonSchemaFactory.byDefault
      .getJsonSchema(JsonLoader.fromString(validSchema.toString))
    schema.validate(JsonLoader.fromString(jsonToValidate.toString))
  }
}
