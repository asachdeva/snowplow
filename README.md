# Json  Schema Validation  Api

## Compile Service
sbt compile

## Run Service
sbt run

## Tech-Stack

- [Http4s](http://http4s.org/) the web server
- [Circe](https://circe.github.io/circe/) for json serialization
- [Doobie](https://github.com/tpolecat/doobie) for database access using HikariTransactor against H2
- [Cats](https://typelevel.org/cats/) for pure FP
- [ScalaTest](https://www.scalatest.org/) for testing
- [PureConfig](https://pureconfig.github.io/docs/) for app config
- Tagless Final for the core domain.


## Issues:
Code Coverage is not where I would like it to be yet

 


